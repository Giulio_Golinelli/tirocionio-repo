<div style="font-size:120%">
    <p style="text-align: right;">
        29/03/2021
    </p>
    <div style="text-align: center;">
        <p>
            <i>Tirocinio formativo</i>
            <br>
            <i>Corso di laurea in Ingegneria e Scienze Informatiche</i>
            <br>
            <i>- Università di Bologna, sede di Cesena -</i>
        </p>
        <h1>Riconoscimento e offuscamento di informazioni sensibili</h1>
        <div style="margin: 35%">
            <p>A cura di:</p>
            <p>
                Golinelli Giulio
                <br>
            <i>(Tirocinante)</i>
            </p>
            <p>
                Maltoni Davide
                <br>
                <i>(Tutor didattico)</i>
            </p>
            <p>
                Canducci Marco
                <br>
                <i>(Tutor aziendale)</i>
            </p>
        </div>
    </div>
    <p style="float: left;">
        Presso: 
        <i>Cyperloop S.R.L.</i>
    </p>
    <p style="float: right;">
        Periodo: 
        <i>23/02/21 - 19/03/21</i>
    </p>
</div>
<div style="page-break-after: always; visibility: hidden"> 
</div>



# 1. Introduzione
Il tirocinio è stato ospitato dall'azienda *Cyperloop S.R.L.*.

## 1.1. Presentazione ente ospitante
*Cyperloop S.R.L.* è un'azienda con sede a Cesena che si occupa di sicurezza informatica e che nasce dai principali gruppi di hacking locali. Il focus di Cyberloop riguarda la sicurezza operativa (OPSEC): la strada migliore per implementare una reale sicurezza aziendale ed aumentare la resilienza contro attacchi cyber, al fine di minimizzare l'impatto tecnico e di business.

L'Azienda lavora con un approccio integrato tra difesa contro incidenti (blue teaming, incident management, data breach detection e mitigation) e simulazione di attacchi (red teaming, targeted attack, APT, penetration testing) perchè ritenuto il modo migliore per massimizzare l’efficacia di entrambi. I servizi offerti sono progettati ad-hoc per realtà sia enterprise sia piccole/medie imprese.

## 1.2. Contesto professionale
Per l'intera durata del tirocinio, si ha lavorato e comunicato in modalità smart working causa virus Covid19.
Ciò non ha però impedito il normale conseguimento delle attività e anzi, è stata un'occasione per uscire dal medio contesto aziendale e potersi confrontare con un approccio già presente in diverse realtà professionali anche prima dell'avvento della pandemia.

## 1.3. Obiettivo del tirocinio
Il tirocinio aveva l'obiettivo di far apprendere le nozioni necessarie, in ambito professionale, riguardanti le informazioni private: cosa sono, come si presentano, quali sono le modalità e rischi di conservazione, come si rilevano, proteggono ed offuscano.
In particolare, le attività si sono concentrate attraverso uno studio supervisionato riguardante il concetto d'informazioni e dati, personali e sensibili, sulla loro individuazione, conservazione e protezione.
È stata successivamente affrontata una corposa esperienza pratica in cui si è realizzato un software, sviluppato interamente dal tirocinante sulle specifiche richieste dall'azienda, appositamente predisposte per un contesto professionale riguardante le tematiche sopra citate.


<div style="page-break-after: always; visibility: hidden"> 
</div>

# 2. Tecnologie
Si ha operato agevolmente su diverse tecnologie sulle quali si sono applicate e apprese altrettante nozioni teoriche con cui si è riusciti a costruire e realizzare il prodotto finale.

Tecnologie e strumenti utilizzati:

- Linux (Sistema operativo)
- Python (Principale linguaggio di programmazione)
- Bash (Secondario linguaggio di programmazione)
- JSON (conservazioni informazioni)
- base64 (conservazioni informazioni)
- Espressioni regolari (Riconoscimento di pattern)
- AES, SHA2, Cesare (Cifrari)
- Markdown (Documentazione)
- Git (Sistema di controllo versione)
- Microsoft Teams (Comunicazione e sincronizzazione)
- Trello (Comunicazione e sincronizzazione)

# 3. Attività
Il tirocinio si è svolto in una prima fase di formazione teorica supervisionata riguardante le tematiche previste e, successivamente, in una seconda fase più pratica e corposa.

## 3.1. Prima fase: Formazione teorica
La formazione teorica è stata svolta per una settimana lavorativa in modo intensivo ma non è mai stata abbandonata del tutto. Fino alla fine del tirocinio si sono revisionati e corretti gli appunti, approfonditi ulteriori aspetti e sono state aggiunte tutte le nozioni ritenute necessarie.
Le conoscenze teoriche apprese sono state documentate in diversi file a cura del tirocinante, come appunti per il futuro e verifica delle competenze.

### 3.1.1. Studio dati personali e sensibili
Si è iniziato raccogliendo tutte le informazioni pertinenti i dati personali e sensibili e la legislazione al riguardo.
Per questo lavoro è risultata inizialmente utile la conoscenza acquisita durante l'esame di "Informatica e diritto" offerto dal corso di laurea.
Con una buona base personale, lo studio è proseguito senza intoppi, comprendendo e approfondendo le fonti ufficiali autorizzate.

Si sono raggiunte le competenze di seguito riassunte:
- Ambito, contesto e modalità dei dati personali
- Differenza tra dati personali e dati sensibili
- Conoscenza di diverse tipologie di dati sensibili
- Norme e principi del GDPR
> *Questo studio ha prodotto il file "dati_sensibili.md"*.

### 3.1.2. Studio di tecniche di offuscamento per dati personali e dati sensibili
In questa parte si sono approfondite le modalità di conservazione, i rischi e le conseguenze di una breccia riguardanti i dati personali/sensibili in un contesto informatico aziendale.
In particolare il ruolo dei *log file* come possibile mezzo di conservazione di tali dati: spesso infatti questi vengono automaticamente aggiunti dalle applicazioni, dai servizi ma anche persino dal sistema operativo stesso e spesso all'insaputa di chi è di competenza.

> *Questo studio ha prodotto il file "metodologie_per_dati_sensibili_in_log.md"*.

### 3.1.3. Studio di approfondimento crittografia
Si è pensato anche di eseguire uno studio approfondito di crittografia per comprendere meglio ed elaborare un buono sistema di offuscamento dei dati.
Si è iniziato dalle nozioni di base crittografiche fino ad arrivare ad approfondire argomenti più complessi.

> *Questo studio ha prodotto il file "crittografia.md"*.

## 3.2. Seconda fase: Sviluppo applicativo
La seconda fase è durata per il resto del tirocinio e ha prodotto diversi programmi e script. Aveva come principale obbiettivo lo sviluppo di un software con specifiche su richiesta del datore di lavoro (tutor aziendale). Il sistema richiesto è stato conseguito dal tirocinante con successo e utilizzabile anche in ambito professionale.

### 3.2.1. Sensible2Mock
*"sensible2mock.py"* è il nome inventato dal tirocinante per il software commissionatogli. È un programma scritto in *Python*.
Sono stati prodotti anche altri programmi dal tirocinante, non espressamente richiesti ma utili per lo sviluppo del software principale e apprezzati dal datore di lavoro.

#### 3.2.1.1. Specifiche
1. Prendere in input un array di file di log
2. Rilevare i dati personali e sensibili al suo interno
3. Sostituirli con dati inventati ma che ne preservino formato (dato originale e dato inventato devono essere omomorfi)
4. Inserire e conservare il dato inventato collegato all'hash del dato sensibile originale all'interno di un file consultabile
5. Criptare in modo sicuro il file contenente le associazioni tra dati inventati e hash dei dati originali

Di seguito sono descritti i metodi di approccio per la realizzazione di ogni specifica, inventati e utilizzati dal tirocinante.

##### 3.2.1.1.1. Prendere in input un array di file di log
I diversi *path* dei file di log da computare vengono passati al software come argomenti nella linea di comando, separati da uno spazio.
Per ognuno di questi, il software:

- Verifica l'effettiva esistenza del file
- Crea un nuovo file temporaneo nella stessa directory che conterrà il nuovo log processato
- Inizia a leggere riga per riga il file.

È stato scelto dal tirocinante questo approccio iterativo (riga per riga) di lettura e scrittura del file poiché si è pensato che i log passati possono essere anche di grandi dimensioni e leggere in una sola volta e memorizzare l'intero documento in memoria potrebbe essere troppo invasivo.

##### 3.2.1.1.2. Rilevare i dati personali/sensibili al suo interno
Il software realizza questa funzionalità attraverso un particolare file modificabile dall'utente chiamato *"regexs.txt"*. Il file segue l'approccio in linea con i comuni *Linux configuration file* e contiene al suo interno pattern di espressioni regolari di diversi dati personali e sensibili con associata una stringa che ne descrive la natura. Questi ultimi vengono letti da *sensible2mock* prima di qualsiasi computazione e inseriti in un dizionario Python *{Descrizione -> regex}* es:

{"Gruppo Sanguigno" -> "(A|B|AB|0)[+-]"}

Tutti i dati che rileverà il software sono dei match con una particolare espressione regolare all'interno del file.
L'utente può aggiungere, modificare o rimuovere qualsiasi espressione regolare e relativa descrizione.
All'interno del file sono già presenti alcune espressioni regolari, create dal tirocinante, che riguardano i seguenti dati:

- Numero di carta di credito
- Numero di telefono
- Gruppo sanguigno
- Sesso
- Indirizzo IP

##### 3.2.1.1.3. Sostituzione con un dato inventato che ne preservi il formato
Il dato che rimpiazzerà l'informazione sensibile originale deve preservarne il formato. Ciò significa che nel caso venga rilevato un numero di telefono, questo andrà sostituito con un altro numero, inventato, ma pur sempre riconducibile a un numero di telefono.
I dati falsi o *"mock-up"* sono generati da un modulo Python terziario, non integrato di default, che si chiama *"exrex"*. Quest'ultimo prende in input una espressione regolare e produce una stringa casuale ma conforme all'espressione regolare in input.
Ogni riga letta dal log originario viene, modificata e riscritta sul file temporaneo.

##### 3.2.1.1.4. inserire e conservare il dato inventato collegato all'hash del dato sensibile originale all'interno di un file consultabile
Il dato sensibile originale viene rilevato ed elaborato il *digest* attraverso una funzione *hash* (*SHA2 256 bit*). Quest'ultimo viene poi scritto in un particolare file chiamato *"database.db"* assieme al relativo *mock-up*, alla descrizione del dato ottenuta dal dizionario e dal percorso del file di log originario.

##### 3.2.1.1.5 Criptare in modo sicuro il file contenente le associazioni tra dati inventati e hash dei dati originali

Il database è un file criptato con cifrario *AES*.

### 3.2.2. AES.py
*AES.py* È un modulo Python creato dal tirocinante che contiene diverse funzioni utili per la cifratura e decifratura attraverso cifrario di blocco AES.
La cifratura utilizzata all'interno del modulo vede come *modo operazionale* l'*EAX* che oltre a garantire la perfetta segretezza garantisce anche la perfetta integrità producendo oltre al cifrato anche un *tag* o *MAC (Message Authentication Code)* che ne verifica la compromissione.

#### 3.2.2.1. AES in sensible2mock
*AES.py* viene utilizzato in diversi script e programmi prodotti dal tirocinante ma in particolare da *sensible2mock*, che vede l'utilizzo del modulo per criptare e decriptare il database *database.db* in modo da garantire un ulteriore layer di protezione ai dati.

Viene sfruttata anche un'altra funzionalità offerta dal modulo che consiste nella memorizzazione del cifrato attraverso una rappresentazione in formato *json* contente come chiavi le descrizioni dei vari campi (*nonce, ciphertext, tag*). In questo modo risulta possibile decifrare il file *database.db* anche da altre applicazioni in possesso della chiave di decifratura interpretando i campi del file *json*.

### 3.2.3. Conservazione della chiave
Il sistema utilizza e conserva la chiave di decriptazione del database all'interno di un particolare file chiamato *key.sec*, in formato *base64*. Questo è solo a scopo dimostrativo, per mostrare e verificare il corretto funzionamento del sistema crittografico implementato, ma l'utente può scegliere le modalità di conservazione e ricezione della chiave che più pensa siano consone.

### 3.2.4. Utils.py
*utils.py* è un modulo Python, non espressamente richiesto ma creato dal tirocinante, che contiene diverse funzioni di utilità che vengono utilizzate da diversi script e programmi tra cui anche il modulo *AES.py* e *sensible2mock*.
Tra le funzionalità più importanti troviamo la scrittura e la lettura di una chiave in *base64* da un file e la computazione di un hash (*SHA2 256 bit*).

### 3.2.5. print_db.py
*print_db.py* è uno script Python di utilità che decripta e stampa il contenuto del database *database.db* in *standard output*. Viene utilizzato sia come strumento di debug ma anche come scorciatoia per visionare il database.

### 3.2.6. search.sh
*search.sh* è uno script sviluppato in *BASH*.
Prende in input un insieme di digest hash e, dopo aver decriptato ed estratto il contenuto del database, cerca ognuno di questi al suo interno. Se trova una corrispondenza, stampa il valore mock-up e tutte le altre informazioni associate al digest (natura del dato sensibile, path del log…). Esegue poi anche un'altra ricerca all'interno del log per il rispettivo valore *mock-up*. Se la ricerca ha esito positivo, stampa anche la linea corrispondente del log.

### 3.2.7. ceasar.py
*ceasar.py* è un programma sviluppato in *Python* che ha subito diverse rivisitazioni durante la fase pratica e costituisce una prima versione di software per l'individuazione e l'offuscamento di dati sensibili e dati personali all'interno dei file di log. È stato lasciato all'interno dei file allegati perché costituisce buona parte delle attività svolte durante il tirocinio: tappa importante per il raggiungimento della soluzione ottimale.
*ceasar.py* nonostante sia perfettamente funzionante e raggiunga gli obbiettivi che gli sono stati prefissati, non è un software da poter usare in ambito professionale in quanto non offre sufficiente sicurezza e non permette di lavorare con determinati dati sensibili spesso richiesti. Per la versione ottimale prendere come riferimento *sensible2mock*.

#### 3.2.7.1. Funzionamento
*sensible2mock.py* è un prodotto derivato dal lavoro che è stato precedentemente svolto in *ceasar.py*, di conseguenza i due software hanno parti di funzionamento comuni:

- lettura dei file di log in input
- sistema di espressioni regolari per l'individuazione dei dati personali/sensibili.

Dopo aver aperto in lettura il file di log in input. *ceasar.py* inizializza un generatore di numeri pseudocasuale con un *seed* generato automaticamente (la standard library di Python in questo caso utilizza il tempo di macchina) oppure con uno passato a linea di comando e inizia a leggere il file. Quando viene individuato un dato sensibile questo viene iterato per carattere generando ogni volta un nuovo intero casuale (minore di 10 per i numeri e 26 per lettere). Quest'ultimo costituisce lo *"shift"* per il cifrario di Cesare. Ogni ordinale di carattere viene sommato (cifratura) o sottratto (decifratura) modulo 10 o 26 (a seconda se numero o lettera). Eventuali simboli nel dato sensibile sono ritenuti come costanti (parte del formato) e non modificati. Il seme d'inizializzazione del generatore di numeri pseudocasuale costituisce la chiave del sistema crittografico: infatti basterà reinizializzare il generatore con il medesimo seme per riottenere la medesima sequenza di numeri casuali e il medesimo risultato sui dati sensibili (con l'ipotesi che i risultati del sistema d'individuazione dei dati sensibili rimangano constanti sul medesimo log file).

#### 3.2.7.2. Difetti
Il funzionamento di *ceasar.py*, oltre a non garantire perfetta integrità dei dati, impedisce di poter lavorare con determinati dati il cui formato richiede un qualsiasi sottoinsieme di numeri o lettere.
Esempio:

> Il formato del gruppo sanguigno accetta solamente le lettere 'A' e 'B', il numero '0' e i simboli '+' e '-'. Se si cifrasse un gruppo sanguigno con *ceasar.py*, la lettera 'A' potrebbe diventare una 'Z' e ciò impedirebbe di ripristinare i valori sensibili originali. Il sistema di cifratura diventerebbe *"one way"* e non corrispondente alle specifiche richieste.

# 4. Conclusioni
Il tirocinio è stato una prima esperienza nel mondo della sicurezza informatica, capace di far vivere diverse dinamiche, azioni e scelte appartenenti solamente a un contesto di cybersecurity. Inoltre mi è piaciuto particolarmente l'attività pratica proposta e spero di poter lavorare di nuovo in questo campo nel futuro. L'azienda è stata sempre molto disponibile e rispettosa degli appuntamenti.

# 5. Bibliografia
- https://owasp.org/www-project-top-ten/2017/A3_2017-Sensitive_Data_Exposure
- https://cwe.mitre.org/data/definitions/798.html
- https://cwe.mitre.org/data/definitions/532.html
- https://www.winmagic.com/blog/when-and-where-should-sensitive-data-be-encrypted/
- https://www.garanteprivacy.it/home/diritti/cosa-intendiamo-per-dati-personali
- https://protezionedatipersonali.it/misure-di-sicurezza