#!/bin/bash

# Giulio Golinelli

# This scripts helps you find the line in a log file containg the mockup data
# referred to the sha256 digest provided. Only base64 UTF-8 decoded form accepted.

if [ $# == 0 ]; then
	echo "USAGE: script.bash sha256.."
	exit
fi

for digest_in in "$@"; do
	echo '> Searching' "$digest_in" 'in database..'
	IFS=$'\t'
	read cat mock digest path <<< $(python3 print_db.py | grep -Fw "$digest_in")
	if [ -n "$cat" ]; then
	       	echo -e "Type:\t$cat"
	       	echo -e "Mock:\t$mock"
	       	echo -e "Path:\t$path"
		grep -n "$mock" "$path"
	else
		echo "Not found"
	fi	
done
