# Giulio Golinelli

import json
from base64 import b64encode, b64decode
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

# Generates a new uniform key
def key_gen():
    return get_random_bytes(16)

# INPUT:
# @raw: sequence of bytes to encrypt
# @key: sequence of uniform bytes
# OUTPUT:
# A tuple (nonce, chiphertext, tag)
def encrypt(raw, key):
    cipher = AES.new(key, AES.MODE_EAX)
    return cipher.nonce, *cipher.encrypt_and_digest(raw)

# Decrypt the input provided with key and verify the result with the tag for integrity
# OUTPUT: the plaintext
def decrypt(nonce, ciphertext, tag, key):
    try:
        cipher = AES.new(key, AES.MODE_EAX, nonce)
        return cipher.decrypt_and_verify(ciphertext, tag)
    except ValueError:
        raise("Ciphertext was tampered!")
    except KeyError:
        raise("Key is not correct")

# OUTPUT: A JSON Stringifies version of nonce cipher and tag
def to_json(nonce, cipher, tag):
    keys = ["nonce", "ciphertext", "tag"]
    values = [ b64encode(x).decode() for x in (nonce, cipher, tag) ]
    return json.dumps(dict(zip(keys, values)))

# INPUT: A JSON stringified
# OUTPUT: the tuple (nonce, cipher, tag)
def from_json(json_str):
    return [ b64decode(x.encode()) for x in json.loads(json_str).values() ]

# Encrypt data as @encrypt() and produces json stringified as @to_json()
# OUTPUT: json stringified
def encrypt_to_json(raw, key): 
    return to_json(*encrypt(raw, key))
    
# Read a valid json as @from_json() and decrypt data as @decrypt()
# OUTPUT: the raw plaintext
def decrypt_from_json(json_str, key):
    return decrypt(*from_json(json_str), key)

# Encrypts as @encrypt_to_json and writes json_str overwriting the file
# It path does not exists, it creates the file
def encrypt_to_json_file(raw, key, path):
    with open(path, "w+") as f:
        f.write(encrypt_to_json(raw, key))

# Decrypts as @decrypt_from_json and reading json_str from file
def decrypt_from_json_file(path, key):
    with open(path, "r") as f:
        return decrypt_from_json(f.read(), key)


"""
class AES:
    #Initialize AES with the key provided or a random one
    def __init__(self, key=None):
        self.key = key

    # Sets the current key to be used
    # Gets an optional input via @key of any size > 0 and generates a valid key with it
    # Or it generates a new random key
    @key.setter
    def key(self, value):
        if not value:
            value = get_random_bytes(16)
        else if not isinstance(value, (bytes, bytearray)):
            value = str(value).encode()
        self.key = SHA256.new(data=value).digest()
    
    def encrypt(self, raw):
        self.cipher = AES.new(key, AES.MODE_EAX)
        self.ciphertext, self.tag = self.cipher.encrypt_and_digest(raw.encode())
        return self.ciphertext, self.tag

    def toFile(chiphertext, tag)

    # Appends to the plaintext a string of x remaing bytes containg chr(x)
    def _pad(s):
        reminder = 16 - len(s) % 16
        return s + reminder * chr(reminder)

    # Reads ord(x) bytes and deletes them from tail of plaintext
    def _unpad(s):
        return s[:-ord(s[-1])]

    def __str__(self):
        return {
            "Key": self.key.decode(),
            "Nonce": self.chiper.nonce,
            "chiphertext": self.chipertext,
            "tag": self.tag
        }
"""
