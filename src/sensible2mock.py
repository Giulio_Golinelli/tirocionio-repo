#!/usr/bin/python3

# Giulio Golinelli

"""
This script search and recognize sensible data inside input file and substitutes it with random mockup data but
preserving the format.
It also write to database.db file to store mock and digest of original sensible data.
This overwrites file's contents!
Look for file "regexs.txt" to edit regexs searched patterns
"""

import sys, re, exrex, AES, os, utils, os.path
from base64 import b64decode, b64encode

# Filename costants
REGEXS_FILE = "regexs.txt"
DB_FILE = "database.db"
KEY_FILE = "key.sec"

# inventare uno script che dato un hash di un numero di telefono lo cerca nel db e ti torva il mock # python ti torna il mock
# inventare unos cript che prende in input dei file e cerca il mock in questi e ti rida la linea #BASH con grep
# inventare uno script che fa l'unione di questi # BASH prima python e poi BASH


def main(argv):
    # checking and collectiong args
    if len(argv) == 0:
        print("USAGE: sensible2mock.py files..", file=sys.stderr)
        sys.exit(2)
    
    # load regexs3
    regexs = {}
    with open(REGEXS_FILE) as f:
        for line in f:
            line = line.replace('\n', '')
            if not line.startswith('#') and line:
                regexs[line] = next(f).replace('\n', '') 

    # load key for db (demostrative)
    key = utils.key_from_file(KEY_FILE)
    
    #load db
    if os.path.isfile(DB_FILE):
        db = AES.decrypt_from_json_file(DB_FILE, key).decode()
    else:
        db = ""

    # main
    for filename in argv:
        try:
            f_in = open(filename, 'r')
        except FileNotFoundError:
            print(filename + " don't found!")
        else:
            print("> searching in " + filename + "..")
            c = 0
            with open(filename+".tmp", "w+") as temp: #overwriting and avoid ram limits
                for line in f_in:
                    spans = [] #for regex match intersection
                    for name, regex in regexs.items():
                        for m in re.finditer(regex, line):
                            if not any(a <= m.start(0) <= b or a <= m.end(0) <= b for a, b in spans):
                                spans.append(m.span(0))
                                mock = exrex.getone(regex)
                                line = line.replace(m[0], mock, 1)
                                db += '\t'.join([name, mock, b64encode(utils.do_hash(m[0])).decode(), filename]) + '\n'
                                c += 1
                    temp.write(line)    
            f_in.close()
            os.rename(filename+'.tmp', filename)
            print(str(c) + " sensible fields found!")

    #write db
    AES.encrypt_to_json_file(db.encode(), key, DB_FILE)

if __name__ == "__main__":
    main(sys.argv[1:]) 
