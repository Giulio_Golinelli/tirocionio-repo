import sys, AES, utils

def main(argv):
    key = utils.key_from_file("key.sec")
    db = AES.decrypt_from_json_file("database.db", key).decode()
    print(db)

if __name__ == "__main__":
    main(sys.argv[1:])
