#!/usr/bin/python3

# Giulio Golinelli

"""
Commentare le righe nel file regexs.txt che riguardano dati sensibili rientranti in un particolare gruppo di stringhe
Poichè una volta criptate non saranno più riconosciute dalle regex
"""

import sys, re, random

regexs = []

def ceasar(word, mode):
    result = ""
    for char in word:
        shift = random.randint(0, 26) * mode
        if char.isnumeric():
            result += chr((ord(char) + shift - 48) % 10 + 48)
        elif char.isalpha():
            if char.isupper():
                result += chr((ord(char) + shift - 65) % 26 + 65)
            else:
                result += chr((ord(char) + shift - 97) % 26 + 97)
        else:
            result += char
    return result

def check(word):
    for regex in regexs:
        if re.match(regex, word):
            return True
    return False

def main(argv):
    # checking and collectiong args
    if len(argv) < 1 or len(argv) > 2 or (argv[0] != "-e" and argv[0] != "-d"):
        print("USAGE: crypt.py -e/-d [key] < inputfile > outputfile", file=sys.stderr)
        sys.exit(2)
    
    # setting mode
    # default encrypt
    mode = 1
    if argv[0] == "-d":
        mode = -1
    
    # setting shift
    key = random.randint(0, 100000);
    if len(argv) == 2:
        key = argv[1]
    random.seed(key)

    # load regexs
    with open("regexs.txt") as re_file:
        for line in re_file:
            line = line.replace('\n', '')
            if not line.startswith('#') and line:
                regexs.append(line)

    # main
    for line in sys.stdin:
        for word in line.split():
            if check(word):
                print(ceasar(word, mode), end=' ')
            else:
                print(word, end=' ')
        print()
    if mode == 1:
        print("file encrypted!", file=sys.stderr)
    else:
        print("file decrypted!", file=sys.stderr)
    print("KEY: " + str(key), file=sys.stderr)

if __name__ == "__main__":
    main(sys.argv[1:])
