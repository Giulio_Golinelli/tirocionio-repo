
# Giulio Golinelli

"""
This file is a collection of utility functions to be used
"""

import AES
from hashlib import sha256
from base64 import b64encode, b64decode

# Compute a sha-2 256 bit over a decoded string
def do_hash(word):
    return sha256(word.encode()).digest()

# Reads a key from a file in its base64 representation
def key_from_file(path):
    with open(path, 'r') as f:
        return b64decode(f.read().encode())

# Writes a binary key to a file in its base64 representation
def key_to_file(key, path):
    with open(path, 'w') as f:
        f.write(b64encode(key).decode())

