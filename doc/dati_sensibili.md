# 1. Dati Personali

<p style="text-align: right; font-style: italic; font-size:150%; margin: auto">;
  <i>Giulio Golinelli</i>
  <br>
  <i>23/02/2021</i>
</p>

## Tabella dei contenuti <!-- omit in toc -->
- [1. Dati Personali](#1-dati-personali)
  - [1.1. Trattamento](#11-trattamento)
  - [1.2. Dati Sensibili](#12-dati-sensibili)
  - [1.3. Il Garante Della Privacy](#13-il-garante-della-privacy)
  - [1.4. Sicurezza](#14-sicurezza)
  - [1.5. Misure Minime](#15-misure-minime)
    - [1.5.1. Misure Organizzative](#151-misure-organizzative)
      - [1.5.1.1. Riservatezza](#1511-riservatezza)
        - [1.5.1.1.1. Gestione degli accessi](#15111-gestione-degli-accessi)
      - [1.5.1.2. Resilienza](#1512-resilienza)
      - [1.5.1.3. Integrità](#1513-integrità)
      - [1.5.1.4. Disponibilità](#1514-disponibilità)
      - [1.5.1.5. Verifica e test](#1515-verifica-e-test)
    - [1.5.2. Misure tecniche](#152-misure-tecniche)
      - [1.5.2.1. Dati Attivi](#1521-dati-attivi)
      - [1.5.2.2. Dati trasmessi a terzi](#1522-dati-trasmessi-a-terzi)
      - [1.5.2.3. Dati archiviati](#1523-dati-archiviati)
      - [1.5.2.4. Offuscamento delle informazioni](#1524-offuscamento-delle-informazioni)
        - [1.5.2.4.1. Pseudonomizzazione](#15241-pseudonomizzazione)
        - [1.5.2.4.2. Cifratura](#15242-cifratura)
  - [1.6. Credenziali](#16-credenziali)
    - [1.6.1. Autenticazione](#161-autenticazione)
      - [1.6.1.1. Tipologie di autenticazione](#1611-tipologie-di-autenticazione)
        - [1.6.1.1.1. Inbound auth](#16111-inbound-auth)
        - [1.6.1.1.2. Outbound auth](#16112-outbound-auth)
    - [1.6.2. Tipologie di credenziali](#162-tipologie-di-credenziali)
      - [1.6.2.1. Password testuale](#1621-password-testuale)
      - [1.6.2.2. Coockie](#1622-coockie)
      - [1.6.2.3. Numero di telefono](#1623-numero-di-telefono)
      - [1.6.2.4. Impronta biometrica](#1624-impronta-biometrica)
  - [1.7. Valutazione d'impatto](#17-valutazione-dimpatto)
  - [1.8. FONTI:](#18-fonti)

I dati personali sono un insieme di informazioni correlate tra loro che identificano o rendono identificabile, una persona fisica.

## 1.1. Trattamento
I dati personali vengono raccolti attraverso un cosiddetto "trattamento" dei dati personali, ovvero una o più qualsiasi operazione sui dati personali, svolta con l'ausilio o meno di processi automatizzati.
Ogni trattamento deve essere conforme alle norme del GDPR (General Data Protection Regulation) e necessita del consenso dell'interessato.

## 1.2. Dati Sensibili
I dati sensibili sono una particolare categoria di dati personali come l'orientamento o adesione a collettivi di tipo politico, filosofico, religioso, la razza, lo stato di salute.
Recentemente sono stati inclusi anche i dati genetici, biometrici e l'orientamento sessuale.
I trattamenti che prevedono la raccolta di dati sensibili necessitano oltre al consenso dell'interessato anche di quello del garante della privacy.

## 1.3. Il Garante Della Privacy
Il garante della privacy è un organo dello stato, autonomo e indipendente, che opera per imporre il rispetto delle norme e dei principi del GDPR.

## 1.4. Sicurezza
Al giorno d'oggi la stragrande maggioranza dei dati personali raccolti attraverso un trattamento è conservata elettronicamente. L'articolo 32 del GDPR impone comunque che il titolare impieghi misure tecniche e organizzative, le cosiddette "misure minime", adeguate per garantire un livello di sicurezza al rischio riscontrato nella valutazione d'impatto.

## 1.5. Misure Minime
Il GDPR divide le misure minime in due categorie:
- Misure Tecniche
- Offuscamento delle informazioni
- Misure organizzative
- Riservatezza
- Resilienza, integrità e disponibilità
- Verifica e test

### 1.5.1. Misure Organizzative
Sono misure che riguardano e stabiliscono, in merito a un sistema informativo, chiunque possa avere accesso ai contenuti, le caratteristiche tecniche e topologiche e procedura di verifica e test.

#### 1.5.1.1. Riservatezza
In ambito informatico, spesso erroneamente tradotta con "confidenzialità" (dal termine inglese "Confidentiality", approvato e definito nello standard ISO/IEC).
La riservatezza è la proprietà dei dati di essere consultati solamente dalle persone autorizzate. Si può garantire riservatezza attraverso un processo di offuscamento e/o di gestione degli accessi.

##### 1.5.1.1.1. Gestione degli accessi
La gestione degli accessi viene applicata sia agli utenti esterni che agli utenti interni al sistema informativo.
L'autenticazione, indipendentemente dalla modalità con cui viene effettuata che può variare a seconda della tipologia e permessi di un utente, è d'obbligo e necessaria previa qualsiasi operazione sui dati, al fine di tracciare qualsiasi azione e risalire chiaramente al suo responsabile (per questo si implementa anche un sistema di logging).
Ogni utente dovrà essere identificato da una o più credenziali univoche (Una combinazione correlata e precisa di username e password, una caratteristica biometrica, l'indirizzo IP ecc..).
La condivisione delle credenziali rende vana qualsiasi metodo di autenticazione e tracciamento e ciò è una violazione della riservatezza.

#### 1.5.1.2. Resilienza
&gt; "La resilienza è la capacità di un sistema di adattarsi al cambiamento".

In informatica significa rendere un sistema capace di reagire a problemi, sia di natura tecnica che fisica, e preservare le proprie funzionalità.
Ciò, in particolare nei sistemi informativi, può essere attuato attraverso meccanismi di gestione di backup, continuità e aggiornamento degli applicativi.

#### 1.5.1.3. Integrità
In informatica si incede rendere impossibile la modificazione del contenuto di qualsiasi dato da soggetti non autorizzati. La certezza del potere informativo.

#### 1.5.1.4. Disponibilità
La disponibilità è la caratteristica che esprime la reperibilità, la facilità e la velocità al momento di usufruire di un determinato insieme d'informazioni.

#### 1.5.1.5. Verifica e test
È necessario predisporre procedure di verifica del corretto funzionamento e attuazione delle misure minime, da utilizzare periodicamente sul sistema informativo.
L'articolo 42 inoltre, del GDPR garantisce il diritto al titolare del trattamento di essere certificato da un ente approvato a manifesto della conformità dei requisiti.

### 1.5.2. Misure tecniche
Per capire quali misure adottare e di conseguenza proteggere i dati è necessario prima di tutto capire come saranno utilizzati e condivisi.

#### 1.5.2.1. Dati Attivi
Dati che vengono utilizzati periodicamente e frequentemente per scopi necessari. Offuscarli significherebbe diminuire la disponibilità di questi e rendere tedioso il loro utilizzo. L'unica misura protettiva per questa categoria è la gestione degli accessi (riservatezza, misura organizzativa).

#### 1.5.2.2. Dati trasmessi a terzi
I dati trasmessi possono essere intercettati; ciò metterebbe in discussione i principi di riservatezza e integrità. Di conseguenza sarà necessario offuscare sempre i dati prima della loro trasmissione, utilizzando protocolli sicuri come il TLS o protocolli che ne fanno uso come l'HTTPS, FTPS, SSH ecc..

#### 1.5.2.3. Dati archiviati
Questa è la categoria su cui più è possibile applicare misure di protezione, anche intense, poiché non è necessaria una elevata disponibilità.
- Minimizzazione della finalità: è uno dei principi fondamentali del GDPR e consiste nel conservare solamente i dati che potrebbero effettivamente servire. Avere meno dati possibili aiuta a ridurre l'impatto di una eventuale condivisione illegale.
- Offuscamento: per tutti i dati archiviati e non attivi è necessario l'offuscamento.

#### 1.5.2.4. Offuscamento delle informazioni
##### 1.5.2.4.1. Pseudonomizzazione
La pseudonomizzazione è una procedura obbligatoria di sicurezza che ha l'obbiettivo di rendere nulla la capacità intrinseca d'identicazione o identificabilità dei dati.
Consiste nel separare i dati ottenuti da ogni trattamento e mantenere memorizzate le due parti in modo dislocato.
Il risultato è che in una eventuale consultazione e/o divulgazione non autorizzata di uno dei due insiemi, si rende impossibile l'identificazione delle persone fisiche che hanno partecipato alla raccolta dati senza la consultazione e/o divulgazione dell'altro insieme di dati.

##### 1.5.2.4.2. Cifratura
Operazioni effettuate sul contenuto dei dati, per renderlo non consultabile a soggetti non autorizzati.
Cifrare i dati significa anche diminuire la loro disponibilità, in quanto ogni soggetto autorizzato potrà consultare il contenuto originale solamente previa decifrazione dei dati, operazione che richiede tempo.

## 1.6. Credenziali
Tutte le credenziali, indipendentemente dalle responsabilità dell'utente a cui sono associate, sono considerate dati sensibili e per questa ragione è necessario proteggerli, ma non sempre è possibile oppure non risolutivo.

### 1.6.1. Autenticazione
L'autenticazione è il processo d'identificazione, verifica e assegnazione di un utente:
- Input delle credenziali (identificazione)
- Comparazione delle credenziali fornite con un insieme di credenziali (verifica)
- Se trovato riscontro, attribuzione di un particolare utente con relative responsabilità (assegnazione)

#### 1.6.1.1. Tipologie di autenticazione
Esistono 2 tipologie di autenticazione ("Authentication" o "auth", in inglese)

##### 1.6.1.1.1. Inbound auth
Non c'è divisione tra front-end e back-end, l'autenticazione sussiste e prende vita all'interno della piattaforma software stessa.
Spesso questa modalità prevede un insieme di credenziali, criptate o meno, all'interno del software (es. all'interno del binario o del codice sorgente se è un sw open) e utilizzate dal software per la fase di verifica.
L'hard-coding delle credenziali è un grosso problema poiché, una volta distribuito il software, utenti malintenzionati scopriranno senza difficoltà i valori di queste credenziali e attraverso internet li condivideranno al mondo intero.
La modalità di distribuzione o l'eventuale protezione delle credenziali nel software può influire sul tempo necessario per manometterle ma non il risultato (la manomissione è certa).
Una possibile soluzione per ovviare a questo problema è non immettere delle credenziali definitive all'interno del software ma lasciare che sia il cliente a crearle per varie tipologie di utenti. In questo modo i clienti non avranno delle uniche credenziali in comune ma ognuno avrà le sue.

Esempi di Inbound Auth:
- pacchetto zip protetto da password (hard-coded, criptata, open-source)
- log-in page su server web che conserva valori non criptati nel codice javascript (hard-coded, non criptata, open-source)
- Microsoft Windowd EFS (hard-coded, non definitiva, criptata, closed-source)

##### 1.6.1.1.2. Outbound auth
In questa modalità di autenticazione il software viene diviso in front-end e back-end:
- Identificazione: lato front-end si prelevano le credenziali
- Verifica: le credenziali vengono inviate al back-end, software che spesso non risiede localmente alla macchina del cliente e verso cui solo utenti autorizzati hanno accesso
- Attribuzione: questa fase inizia lato back-end dopo l'avvenuto riscontro positivo con le credenziali ma viene poi finisce lato front-end con il riconoscimento del cliente dell'utente ottenuto

Esempi di outbound auth:
- Log-in in una applicazione web

### 1.6.2. Tipologie di credenziali
Recentemente le credenziali hanno subito una rivoluzione e spaziano dalla classica password, alle credenziali "trasparenti" fino alle credenziali fortemente leganti alle caratteristiche personali del cliente.

#### 1.6.2.1. Password testuale
la forma di credenziale più sicura se utilizzata nel modo corretto (password sufficientemente lunga con ampia varianza di caratteri).

#### 1.6.2.2. Coockie
Anche chiamato anonymous login è una credenziale "trasparente" poiché il cliente la possiede ma non è di norma a conoscenza del valore. Viene utilizzata spesso nelle applicazioni web e mobile dove il front-end salva localmente la credenziale e la passa automaticamente al back-end al momento della autenticazione. Il fatto che viene salvata localmente nella macchina del cliente riduce la sicurezza di questa forma di autenticazione poiché esperti malintenzionati potrebbero consultarla illegalmente usufruendosi di altre vulnerabilità.

#### 1.6.2.3. Numero di telefono
Viene spesso utilizzato per la autenticazione a due fattori e per il recupero delle credenziali. Al giorno d'oggi la maggior parte delle multinazionali da per scontato che il numero di telefono è una caratteristica privata e identificante di una persona. Anche il numero di telefono presenta delle vulnerabilità in quanto potrebbe essere clonato oppure un malintenzionato potrebbe avere facile accesso al cellulare della vittima, sia fisicamente che tecnologicamente nel caso di una vittima poco esperta (social engeneering).

#### 1.6.2.4. Impronta biometrica
Le impronte biometriche come il fingerprint o l'iris recognition sono caratteristiche private individuali di una persona.
Il più grande vantaggio è la velocità di autenticazione ma è anche la sua più grande vulnerabilità in quanto è l'unica forma di autenticazione a non avere un processo di verifica deterministico.
Finita la fase d'identificazione l'impronta digitale ottenuta viene convertita in una rappresentazione numerica e confrontata con quella salvata localmente nella macchina. Se i due numeri corrispondono con una sufficiente precisione allora viene attribuito l'utente.
Ciò espone una vulnerabilità in quanto non è più necessario avere l'impronta digitale a cui è associato l'utente di cui si vuole entrare in possesso, bensì ne basta una abbastanza simile. Autenticazioni simili per questa ragione si rendono molto vulnerabili ad attacchi brute-force.

## 1.7. Valutazione d'impatto
L'articolo 35 del GDPR definisce come DIPA (Data Protection Impact Assesment) o valutazione d'impatto come una procedura obbligatoria previa qualsiasi trattamento o immagazzinazione di dati personali, sostenuta dal titolare con l'aiuto del DPO (Data Protection Officer) e del CISO (Chief Information Security Officer) atta a valutare i rischi e approntare misure idonee per affrontarli.

## 1.8. FONTI:
- Esame d'Informatica e Diritto
- https://it.wikipedia.org/wiki/Trattamento_dei_dati_personali
- https://it.wikipedia.org/wiki/Dati_sensibili
- https://www.garanteprivacy.it/home/diritti/cosa-intendiamo-per-dati-personali
- https://protezionedatipersonali.it/misure-di-sicurezza
- https://it.wikipedia.org/wiki/Confidenzialit%C3%A0
- https://it.wikipedia.org/wiki/Resilienza
- https://it.wikipedia.org/wiki/Resilienza
- https://owasp.org/www-project-top-ten/2017/A3_2017-Sensitive_Data_Exposure
- https://cwe.mitre.org/data/definitions/798.html
- https://it.wikipedia.org/wiki/Sistema_di_riconoscimento_biometrico