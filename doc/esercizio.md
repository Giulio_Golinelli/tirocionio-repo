
# 1. Esericzio

- [1. Esericzio](#1-esericzio)
  - [1.1. Specifiche](#11-specifiche)
    - [1.1.1. Premessa:](#111-premessa)
    - [1.1.2. Mantenere il formato](#112-mantenere-il-formato)
  - [1.2. Metodi di approccio alla identificazione](#12-metodi-di-approccio-alla-identificazione)
    - [1.2.1. Opzione A:](#121-opzione-a)
      - [1.2.1.1. Vantaggi:](#1211-vantaggi)
      - [1.2.1.2. Svantaggi:](#1212-svantaggi)
    - [1.2.2. Opzione B](#122-opzione-b)
      - [1.2.2.1. Vantaggi:](#1221-vantaggi)
      - [1.2.2.2. Svantaggio:](#1222-svantaggio)
      - [1.2.2.3. Soluzione](#1223-soluzione)
  - [1.3. Metodi di approccio all'offuscamento](#13-metodi-di-approccio-alloffuscamento)
    - [1.3.1. Algoritmo di offuscamento](#131-algoritmo-di-offuscamento)
    - [1.3.2. Analisi dell'algoritmo di offuscamento](#132-analisi-dellalgoritmo-di-offuscamento)

## 1.1. Specifiche
Si richiede di costruire un software che preso in input un array di stringhe, se queste corrispondono a un dato sensibile, cifrarle mantenendo il formato originale chiaramente distinguibile.

### 1.1.1. Premessa:
Mantenere un formato riconoscibile significa utilizzare uno schema crittografico molto malleabile che non potrà rispettare la definizione di indistinguibilità e segretezza, assolutamente inadatto in fase di produzione.

### 1.1.2. Mantenere il formato
Mantenere il formato dopo la cifratura significa poter risalare alla natura del campo ma non al suo valore orignale.  
In particolare:
 - tutti i caratteri costanti in un campo vanno mantenuti costanti (Spazi, simboli, es: il + nel prefisso telefonico)
 - la tipologia di un carattere non può cambiare (le lettere devono rimanere lettere, i numeri devono rimanere numeri)

## 1.2. Metodi di approccio alla identificazione
Gli svantaggi di una opzione escludono il vantaggio dell'altra (Nessuno vantaggio/svantaggio ripetuto)

### 1.2.1. Opzione A:
È disponibile uno schema del log che viene consultato per capire la natura dei campi letti e se eventualmente doverli criptare
#### 1.2.1.1. Vantaggi:
 - Meno laborioso (Non è necessario dedurre il formato)
#### 1.2.1.2. Svantaggi:
 - Lo schema del log da solo informazioni sulla natura dei campi ma non sul modo in cui criptarli (È utile solamente in fase di identificazione)

### 1.2.2. Opzione B
Il software utilizza in serie diverse tipologie di RegEx costruite appositamente per intercettare determinati formati che corrispondono a dati sensibili.
#### 1.2.2.1. Vantaggi:
 - Possibilità di criptare qualsiasi log indipendentemente dall'ordine dei campi (non è necessario sapere lo schema del log)
 - Logica concentrata in un solo file (Dover consultare lo schema in un altro file crea una dipendenza e separa la logica)
#### 1.2.2.2. Svantaggio:
Dati due campi di natura diversa (uno sensibile e l'altro no) con lo stesso formato, verranno criptati entrambi.  
Esempio: 
 - Beatrice (Nome, non sensibile)
 - Induismo (Religione, sensibile)
Entrambi hanno lo stesso formato (Es: "tra 4 e 8 lettere, prima lettera maiuscola").  
#### 1.2.2.3. Soluzione
 - Caso 1: Lo spazio dei possibili valori dei due campi ha intersezione nulla:  
   È necessario confrontare il valore con un set di possibili valori di quel dato sensibile.  
   Esempio: "Beatrice" ha lo stesso formato di religione e per questo viene confrontato con il valore di tutte le religioni possibili per capire che non è una religione.
 
 - Caso 2: Lo spazio dei possibili valori dei due campi ha una intersezione non nulla:   
   È impossibile identificare correttamente senza lo schema.  
   Esempio:  
   - "Giulio" (Nome, non sensibile)
   - "ZXVQRT" (Dato sensibile e causale in tutta la sua estensione)  
   In questo esempio è impossibile essere certi che "Giulio" non possa essere effettivamente un valore del secondo campo.

## 1.3. Metodi di approccio all'offuscamento
Dato il requisito di conservazione del formato non è possibile trattare tutti i campi come sequenze di byte e cifrarli indistintamente.
Inoltre per il requisito di mantenimento della tipologia del carattere è possibile implementare solamente un algoritmo ROT come cifratura.
In particolare:
 - I numeri verranno cifrati con un algoritmo Ceaser Cipher mod 5
 - Le lettere verranno cifrate con un algoritmo Ceaser Cipher mod 13

### 1.3.1. Algoritmo di offuscamento
 - Viene generata una chiave casuale per criptare i numeri
 - Viene generata una chiave casuale per criptare le lettere
 - Viene selezionata solamente la parte variabile del campo
 - per ogni carattere si individua la tipologia (numero, lettera) e si cifra di conseguenza

### 1.3.2. Analisi dell'algoritmo di offuscamento
Gli attacchi conosciuti contro il ceaser cipher sono di 3 tipi:
 - Analisi di frequenza
 - ricerca estesa
 - known plaintext
Tutti questi punti deboli dello schema vengono completamente annullati dalla casualità intrinseca nel plaintext stesso: un attaccante che non ha informazioni aggiuntive se non il ciphertext non può sapere quale poteva essere la sequenza di caratteri del plaintext originale.
È comunque uno schema insicuro da un punto di vista dell'integrità dei dati: dal momento che il formato è identificabile è facile poter cambiare il contenuto cifrato e cambaire anche il contenuto originale. Questa è una caratteristica tipica degli schemi crittografici molto malleabili.