# Appunti di crittografia
<p style="text-align: right; font-style: italic; font-size:150%; margin: auto">
<i>Giulio Golinelli</i>
<br>
<i>23/02/2021</i>
</p>

- [Appunti di crittografia](#appunti-di-crittografia)
- [2. Modelli di attacco](#2-modelli-di-attacco)
- [3. Nomenclatura](#3-nomenclatura)
- [4. Schema crittografico a chiave privata](#4-schema-crittografico-a-chiave-privata)
- [5. Definizioni](#5-definizioni)
  - [5.1. Sicurezza](#51-sicurezza)
  - [5.2. Perfetta segretezza (Informale)](#52-perfetta-segretezza-informale)
  - [5.3. Perfetta segretezza (formale)](#53-perfetta-segretezza-formale)
  - [Requisito lunghezza chiave perfetta segretezza](#requisito-lunghezza-chiave-perfetta-segretezza)
  - [5.4. Perfetta indistinguibilità](#54-perfetta-indistinguibilità)
  - [5.5. Segretezza computazionale](#55-segretezza-computazionale)
    - [Approccio asintotico](#approccio-asintotico)
  - [Definizione asintotica di pseudorandomicità](#definizione-asintotica-di-pseudorandomicità)
  - [Generatori pseudorandom (PRG)](#generatori-pseudorandom-prg)
  - [Sicurezza-CPA](#sicurezza-cpa)
  - [Pseudo Random Functions (PRF)](#pseudo-random-functions-prf)
  - [Pseudo Random Generator (PSG)](#pseudo-random-generator-psg)
    - [Cifrari di blocco](#cifrari-di-blocco)
      - [Chipher block mode](#chipher-block-mode)
        - [Counter mode](#counter-mode)
        - [CBC](#cbc)
  - [Integrità](#integrità)
    - [MAC](#mac)
      - [CBC-MAC](#cbc-mac)
      - [HMAC](#hmac)
  - [Crittografia a chiave pubblica](#crittografia-a-chiave-pubblica)
    - [Teoria dei numeri](#teoria-dei-numeri)
      - [Teorema di Fermat](#teorema-di-fermat)
        - [Corollario](#corollario)
- [6. Schemi crittografici](#6-schemi-crittografici)
  - [6.1. Onte time pad](#61-onte-time-pad)
    - [6.1.1. Schema](#611-schema)
    - [6.1.2. Prova della perfetta segretezza](#612-prova-della-perfetta-segretezza)
    - [6.1.3. Svantaggi](#613-svantaggi)
- [7. Fonti](#7-fonti)

# 2. Modelli di attacco
- Known plaintext attack
- Know ciphertext attack
- chosen plaintext attack
- chosen chiphertext attack

# 3. Nomenclatura
- M: variabile aleatoria che rappresenta il messaggio in chiaro. Indicheremo con m il valore che quest'ultima potrebbe assumere.
- **M**: spazio contenente tutti i possibili messaggi in chiaro
- C: variabile aleatoria che rappresenta il messaggio cifrato. Indicheremo con c il valore che quest'ultima potrebbe assumere.
- **C**: spazio contente tutti i possibili messaggi cifrati
- K: variabile aleatoria che rappresenta la chiave. Ha una distribuzione uniforme. Indicheremo con k il valore che quest'ultima potrebbe assumere.
- **K**: spazio contente tutte le possibili chiavi

# 4. Schema crittografico a chiave privata
Uno schema crittografico a chiave privata è definito da:
- k Gen(): un algoritmo generatore di chiave casuale
- c Enc(m, k): un algoritmo di cifratura che utilizza la chiave k e il messaggio per produrre un testo cifrato c
- m Dec(c, k): un algoritmo di decifratura che utilizza la chiave k e il testo cifrato c per produrre il messaggio originale m

# 5. Definizioni
## 5.1. Sicurezza
Indipendentemente dalle informazioni possedute dall'attaccante prima d'intercettare il messaggio cifrato, quest'ultimo non deve rivelare alcun tipo d'informazione aggiuntiva.

## 5.2. Perfetta segretezza (Informale)
Uno schema crittografico si dice perfettamente segreto se la distribuzione di probabilità di M conosciuta dall'attaccante non varia anche dopo l'intercettazione di c.

## 5.3. Perfetta segretezza (formale)
Pr(M = m | C = c) = Pr(M = m)

## Requisito lunghezza chiave perfetta segretezza
Uno schema per essere perfettamente segreto è necessario che la dimensione dello spazio delle chiavi **K** sia maggiore o uguale a quella dei messaggi **M**
- Supponiamo che **K** < **M**
- M è uniforme: Pr(M = m) = 1/|**M**| > 0
- **C** <= **K** < **M**: c'è almeno un messaggio m' che non può corrispondere a un c
- Pr( M = m' | C = c) = 0 !-> Pr(M = m')

## 5.4. Perfetta indistinguibilità
Esperimento randomico(n)
Pi = (Gen, Enc, Dec)
A = Attaccante
- A sceglie 2 messaggi di uguale lunghezza m0, m1
- k Gen(1^n), b 0, 1}, cEnc(mb)
- b'
Uno schema crittografico si dice perfettamente indistinguibile se A riesce a indovinare con probabilità esattamente uguale a 1/2
'Pr(b' = b) = 1/2'
La definizione di perfetta indistinguibilità è un altro modo per definire la perfetta segretezza.

## 5.5. Segretezza computazionale
Si tratta di un rilassamento alla perfetta segretezza per eliminare i suoi svantaggi nel mondo moderno (chiave lunga quanto il messaggio).
La chiave non deve essere lunga quanto il messaggio ma ciò comporta che la probabilità d'indovinare la chiave in una ricerca esaustiva non sarà più uguale a 1/2.

### Approccio asintotico
Si definisce un parametro di sicurezza N, un numero positivo, in comune tra le parti e di pubblico dominio (l'attaccante ne è a conoscenza).
Si può pensare N come la lunghezza della chiave.
Si rilassa la perfetta segretezza in linea con la segretezza computazionale:
- la probabilità d'indovinare il messaggi con una ricerca esaustiva è 1/2 + un numero trascurabile in N (minore di 1/p(n))
- Si considerano solo attaccanti con capacità computazionale polinomiale in N (algoritmi che costano al massimo un polinomio di n)

## Definizione asintotica di pseudorandomicità
Sia:

- n il parametro di sicurezza (lunghezza delle stringhe di bit)
- A un algoritmo probabilistico polinomiale in n.
- x = {0, 1}^n
- Di una distribuzione di stringhe di lunghezza polinomiale in i
- Dn una successione di D fino a N (si contano tutte le stringhe polinomiali in i fino a n)
| PrDn(A(x) = 1) - PrUp(n)(A(x) = 1) | <= epsilon
dove epsilon è un numero trascurabile in n ovvero:
epsilon <= 1/p(n)

## Generatori pseudorandom (PRG)
Un generatore pseudorandom è un algoritmo efficiente e deterministico che trasforma un piccolo seme uniforme in un lunga sequenza pseudorandom.
Sia:

- Dn una distribuzione di stringhe lunghe p(n) generate dall'algoritmo con il seed uniforme
- y una di queste stringhe
- x una stringa qualsiasi
Allora:
PrDn[y] = PrUn[G(x) = y] = Somx: G(x) = y PrUn[x] = Somx: G(x) = y 2^-n
= |{x: G(x) = y}| / 2^-n

Non è dimostrata effettivamente l'esistenza dei PRG, poterlo fare significherebbe anche dimostrare che P = NP.
Nonostante questo si può assumere che un particolare algoritmo sia un PRG.
È stato proceduto in questo modo e si è dimostrato in pratica che alcuni algoritmi potrebbero effettivamente essere dei PRG.

## Sicurezza-CPA
Uno schema crittografico si dice sicuro contro i chosen plaintext attack se la probabilità che un attaccante indovini il messaggio criptato è sempre minore o uguale a 1/2 + epsilon(n), nonostante l'attaccante abbia la possibilità di criptare e consultare ciò che vuole. (i plaintext scelti e/o i chiphertext osservati non danno informazioni sui messaggi originali).
Per soddisfare questa definizione è necessario che lo schema includa all'interno di ogni chiphertext prodotto una parte abbastanza grande di casualità di modo tale da rendere la differenza tra un chiphertext e una sequenza di bit uniforme trascurabile in n.

## Pseudo Random Functions (PRF)
Sia Funcn l'insieme di tutte le possibili funzioni che preso in input una precisa stringa di n bit restituiscono un'altra stringa di n bit, scelta in modo uniforme.

Ogni fn avrà 2^n elementi e una sola precisa sequenza di n bit come input (f diverse hanno input diversi).
Esistono 2^n possibili input di conseguenza Funcn avrà 2^(n^2^n) elementi.

Una funzione pseudo-randomica keyed in n, prende in input una chiave uniforme n e un string n e restituisce un'altra stringa n.
La funzione pseudo-randomica si comporta come l'insieme Funcn, con differenze trascurabili in n.

## Pseudo Random Generator (PSG)

Un generatore di casualità utilizza una prf per generare un particolare input.
Viene inizializzato con un seme che è un sequenza di n bit uniforme e gli viene dato in input una qualsiasi stringa.

### Cifrari di blocco
I cifrari di blocco prendono in input un blocco di n bits e Attraverso diverse operazioni producono un testo cifrato e segreto di n bits.
In genere prendono in input un sequenza di n bits pseudocasuale (chiamato initialization vector o nonce) a lo xorano con il blocco.
Se il modulo della lunghezza del messaggio e la lunghezza del blocco non è uguale a 0, si aggiungo i bit rimanente come padding.

#### Chipher block mode
Le modalità di cifratura di blocco aggiungono sicurezza al messaggio cifrato in quanto il contenuto di un blocco cifrato dipende da quello precedente.

##### Counter mode
In questa modalità per il primo blocco viene utilizzato un IV.
Per il secondo blocco si utilizza l'incremento dell'IV utilizzato precedentemente.

##### CBC
Chipher Block Chaining: il cifrato è xorato con il cifrato precedente.

## Integrità
Nonostante aver dimostrato e raggiunto la perfetta segretezza attraverso l'utilizzo della casualità, i messaggi cifrati possono comunque essere manomessi dall'attaccante, producendo sequenze prevedibili o casuali ma comunque diverse da quelle originali. Per evitare questo, è necessario implementare un meccanismo d'integrità dei messaggi cifrati.
In genere consiste nella produzione di un MAC che viene inviato insieme al messaggio cifrato.
In questo modo una modifica al cifrato non verificherà il MAC ricevuto e viceversa (o succedere con probabilità trascurabile in n).

### MAC
Message Authentication Code: Un codice rappresentativo dell'interno messaggio cifrato ottenuto attraverso diversi algoritmi.
Un algoritmo di MAC è ritenuto valido se la probabilità che due messaggi differenti producono lo stesso MAC è trascurabile in n.

#### CBC-MAC
Il MAC viene creato attraverso la cbc mode con IV 0. La chiave della cifratura del messaggio e quella della creazione del mac devono essere diverse.

#### HMAC
Attraverso funzioni di hash come SHA2 o SHA3 o MD5 viene prodotto un codice univoco del messaggio.

## Crittografia a chiave pubblica
### Teoria dei numeri
**Notazione**:
- a = b mod n: significa che in modulo n, a è uguale a b ([a mod n] = [b mod n])
- a = [b mod n]: significa che a è uguale al resto della divisione tra b e n

Un insieme di numeri interi {0..n} A si dice che è un gruppo Abeliano su una determinata operazione binaria $ se:
- È chiuso (a, b in A) allora a $ b è sempre in A)
- Esiste una Identità e in A
- Ogni numero è invertibile (a $ b = e)
- Esiste la commutatività: (a $ b = b $ a)
- Esiste la associatività: (a $ (b $ c) = (a $ b) $ c)

Se n è primo allora tutti i numeri da 1 a n-1 sono invertibili
Se n è il prodotto di due numeri primi p e q allora non è un insieme abeliano. È necessario eliminare dall'insieme tutti i multipli di p e q. ((q-1) e (p-1) rispettivamente).
Una volta tolti, fi(n) è il grado dell'insieme (il numero di elementi) e in questo caso sarà (p-1)(q-1).

#### Teorema di Fermat
a^fi(n) = 1 mod n

##### Corollario
a^x = a^[x mod n]
**Dimostrazione**
Sia m il grado dell'insieme.
Si può esprimere x come q volte m + un certo resto r:
a^x = a^(mq+r) = a^m^(q+r) = 1^q+r = 1^r

# 6. Schemi crittografici
## 6.1. Onte time pad
- Chiave privata
- Provato segretamente perfetto da Shannon

### 6.1.1. Schema
- M = {0, 1}^n
- Gen() = uniform key in {0, 1}^n
- Enc(x, k) = Dec(x, k) = x XOR k

### 6.1.2. Prova della perfetta segretezza
M = {0, 1}^n con distribuzione uniforme -> Pr(M = m) = 2^-n
K = {0, 1}^n con distribuzione uniforme -> Pr(K = k) = 2^-n
Pr(C = c) = Som(m')(Pr(M = m' | C = c) * Pr(M = m')) = Som(m')(Pr(K = c XOR m') * Pr(M = m')) = Som(m')(2^-n * Pr(M = m')) = 2^-n = Pr(M = m)!

### 6.1.3. Svantaggi
- La chiave deve essere lunga quanto il messaggio
- È necessario generare una nuova chiave a ogni nuovo messaggio altrimenti non è garantita la perfetta segretezza

# 7. Fonti
- https://www.winmagic.com/blog/when-and-where-should-sensitive-data-be-encrypted/
- https://www.getapp.com/resources/common-encryption-methods/
- https://en.wikipedia.org/wiki/Homomorphic_encryption
- https://en.wikipedia.org/wiki/Homomorphism
- https://en.wikipedia.org/wiki/Malleability_(cryptography)
- https://en.wikipedia.org/wiki/Semantic_security
- https://en.wikipedia.org/wiki/Chosen-ciphertext_attack
- https://en.wikipedia.org/wiki/Adaptive_chosen-ciphertext_attack
- https://en.wikipedia.org/wiki/Security_parameter
- https://en.wikipedia.org/wiki/Modular_arithmetic