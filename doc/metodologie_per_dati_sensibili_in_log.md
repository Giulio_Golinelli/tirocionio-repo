# 1. Dati sensibili e file di log

<p style="text-align: right; font-style: italic; font-size:150%; margin: auto">;
  <i>Giulio Golinelli</i>
  <br>
  <i>23/02/2021</i>
</p>

Non sempre è necessario perforare un database o eseguire complicati attacchi XSS per rubare le credenziali di uno o potenzialmente più utenti.
Un utente malintenzionato che ha ottenuto segretamente e illegalmente accesso a un sistema potrebbe consultare i file di log e trovare diverse importanti credenziali malauguratamente salvate al suo interno all'insaputa di chiunque.

- [1. Dati Personali](#1-dati-personali)

# Tipologie di file di log
I file di log si possono trovare in qualsiasi parte dell'albero dei file di un filesystem.
Per la gerarchia dei file di linux, i file di log si trovano comunemente nel percorso "/var/log/" ma è anche possibile trovarli all'interno di "/etc/" oppure in "~".
I file di log possono essere generati da varie fonti:
- applicativi: qualsiasi applicazione di uso comune
- servizi: anche chiamati demoni, sono applicativi costantemente in esecuzione in background e spesso riguardano l'interfacciamento I/O con l'interno e l'esterno della macchina (server web, ssh, ftp).
- eventi: moltissime operazioni all'interno della macchina possono generare comunicazioni, errori o avvisi che vengono registrati (i messaggi del kernel <i>kern.log</i> oppure il resoconto delle operazioni di boot <i>boot.log</i>)

# Tecniche per mantenere i logs puliti dai dati sensibili

## Permessi
I file di log non sono criptati poiché ne ridurrebbe la disponibilità e inoltre si sottovaluta la loro potenzialità di danno e li si lascia con i permessi di default, in questo scenario non servirebbe nemmeno che il malintenzionato fosse in possesso di un account di root per consultare e rubare eventuali credenziali.

## DBMS
Spesso viene usato un dato sensibile e identificativo come chiave primaria nelle tabelle dei DB per mantenere le informazioni di un utente.
In questo scenario, nel caso di errori o avvisi dopo una istruzione riguardante la tabella in questione potrebbe registrarsi eni file di log anche il valore della chiave primaria della tupla dove è avvenuto l'errore.
Per ovviare a questo problema è possibile separare il dato sensibile in una tabella a parte contenente il dato e un codice identificativo univoco autogenerato e introdurre una vista nello schema fisico del dbms per reperire facilmente il codice dato il dato sensibile.
La vista è una operazione semplice che è difficile generi errori mentre tutte le altre tabelle possono riferirsi all'utente utilizzando il codice e nell'eventualità di un errore verrà registrato quest'ultimo.

## REST API
Un web server ospitante una rest api utilizza un dato sensibile come chiave per inviare informazioni di un particolare utente.
In questo scenario il più probabile endpoint che si potrebbe creare potrebbe essere "/users/{dato-sensibile}".
Tutti i server web loggano qualsiasi richiesta insieme al rispettivo URL e ciò significherebbe sicuramente salvare informazioni privati nei log.
Per risolvere questa problematica è possibile, insieme alla soluzione al punto precedente, utilizzare un codice identificativo autogenerante per ogni utente.

## Serializzazione
I sistemi moderni implementano in modo autonomo il risultato della serializzazione di una classe.
Se non viene sovrascritta, potrebbero comparire nei file di log tutti i campi trasformati in stringa di una classe, compresi eventuali, dati sensibili.
È sempre buona norma fare un override della procedura di serializzazione di una classe (toString()), riferendosi a questa solamente con il codice identificativo autogenerato.

## Logger
Stampare le informazioni (printf, console.log) è una metologia di debugging che non è corretto usare in un software in produzione.
Spesso si crea invece una classe apposita (logger) che serializza dati strutturati. In questo modo è possibile applicare dei controlli ai dati prima che questi vengano stampati e implimentare filtri per eventuali campi sensibili.

# Fonti
- https://cwe.mitre.org/data/definitions/532.html